const express = require('express')
const jwt = require('jsonwebtoken');
const cors = require('cors')
const app = express()

var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions))

app.get('/', function (req, res) {
  res.send('Hello World!')
})
app.all('/token', function (req, res) {
  var cleanToken = req.headers.authorization.replace(/^BEARER\s/, '')
  var decoded = jwt.verify(cleanToken, 'shhhhh');
  console.log(cleanToken);
  console.log(decoded);
})

app.listen(3001, function () {
  console.log('Token receiver listening on port http://localhost:3001 !')
})
